package fr.episen.pds.ing2.techcity.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import fr.episen.pds.ing2.techcity.models.Historique_Capteur;
import fr.episen.pds.ing2.techcity.models.User;
import fr.episen.pds.ing2.techcity.repositories.Historique_CapteurRepository;
import fr.episen.pds.ing2.techcity.services.Historique_CapteurService;
import fr.episen.pds.ing2.techcity.services.UserService;

@Controller
public class UserController {
	
	 @Autowired
	    private UserService userService;
	 	private Historique_CapteurService historiqueCapteurService;
	 	private Historique_CapteurRepository historiqueCapteurRepository;

	    // display list of users
	    @GetMapping("/")
	    public String viewHomePage(Model model) {
	        model.addAttribute("listUsers", userService.getAllUsers());
	        return "index";
	    }
	    
	    @GetMapping("/showNewUserForm")
	    public String showNewUserForm(Model model) {
	        // create model attribute to bind form data
	        User user = new User();
	        model.addAttribute("user", user);
	        return "new_user";
	    }

	    @PostMapping("/saveUser")
	    public String saveUser(@ModelAttribute("user") User user) {
	        // save user to database
	        userService.saveUser(user);
	        return "redirect:/";
	    }

	    @GetMapping("/showFormForUpdate/{id}")
	    public String showFormForUpdate(@PathVariable(value = "id") long id, Model model) {

	        // get user from the service
	        User user = userService.getUserById(id);

	        // set user as a model attribute to pre-populate the form
	        model.addAttribute("user", user);
	        return "update_user";
	    }

	    @GetMapping("/deleteUser/{id}")
	    public String deleteUser(@PathVariable(value = "id") long id) {

	        // call delete user method 
	        this.userService.deleteUserById(id);
	        return "redirect:/";
	    }

}
