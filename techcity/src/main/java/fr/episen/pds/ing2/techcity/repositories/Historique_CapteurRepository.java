package fr.episen.pds.ing2.techcity.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.episen.pds.ing2.techcity.models.Historique_Capteur;

@Repository
public interface Historique_CapteurRepository extends JpaRepository <Historique_Capteur, Long>{

}
