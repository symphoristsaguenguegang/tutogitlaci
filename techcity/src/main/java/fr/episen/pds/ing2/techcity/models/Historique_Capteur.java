package fr.episen.pds.ing2.techcity.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "Historique_Capteur")
public class Historique_Capteur {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "id_capteur")
	private int id_capteur;
	
	@Column(name = "seuil_min_CO2")
	private int seuil_min_CO2;
	
	@Column(name = "seuil_max_CO2")
	private int seuil_max_CO2;
	
	@Column(name = "seuil_min_NO2")
	private int seuil_min_NO2;
	
	@Column(name = "seuil_max_NO2")
	private int seuil_max_NO2;
	
	@Column(name = "seuil_min_SO2")
	private int seuil_min_SO2;
	
	@Column(name = "seuil_max_SO2")
	private int seuil_max_SO2;
	
	@Column(name = "seuil_min_PM25")
	private int seuil_min_PM25;
	
	@Column(name = "seuil_max_PM25")
	private int seuil_max_PM25;
	
	@Column(name = "température")
	private int température;
	
	@Column(name = "date_time")
	private Timestamp date_time;
	
	@Override
	public String toString() {
		return "Capteur [id=" + id + ", seuil_min_CO2=" + seuil_min_CO2 + ", seuil_max_CO2=" + seuil_max_CO2
				+ ", seuil_min_NO2=" + seuil_min_NO2 + ", seuil_max_NO2=" + seuil_max_NO2 + ", seuil_min_SO2="
				+ seuil_min_SO2 + ", seuil_max_SO2=" + seuil_max_SO2 + ", seuil_min_PM25=" + seuil_min_PM25
				+ ", seuil_max_PM25=" + seuil_max_PM25 + ", température=" + température + ", date_time=" + date_time
				+ "]";
	}

	public Historique_Capteur() {}
	
	public Historique_Capteur(int seuil_min_CO2,int seuil_max_CO2,int seuil_min_NO2,int seuil_max_NO2,int seuil_min_SO2,int seuil_max_SO2,int seuil_min_PM25,int seuil_max_PM25,int température,Timestamp date_time) {
		this.seuil_min_CO2 = seuil_min_CO2;
		this.seuil_max_CO2 = seuil_max_CO2;
		this.seuil_min_NO2 = seuil_min_NO2;
		this.seuil_max_NO2 = seuil_max_NO2;
		this.seuil_min_PM25 = seuil_min_PM25;
		this.seuil_max_PM25 = seuil_max_PM25;
		this.température = température;
		this.date_time = date_time;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId_capteur() {
		return id_capteur;
	}

	public void setId_capteur(int id_capteur) {
		this.id_capteur = id_capteur;
	}

	public int getSeuil_min_CO2() {
		return seuil_min_CO2;
	}

	public void setSeuil_min_CO2(int seuil_min_CO2) {
		this.seuil_min_CO2 = seuil_min_CO2;
	}

	public int getSeuil_max_CO2() {
		return seuil_max_CO2;
	}

	public void setSeuil_max_CO2(int seuil_max_CO2) {
		this.seuil_max_CO2 = seuil_max_CO2;
	}

	public int getSeuil_min_NO2() {
		return seuil_min_NO2;
	}

	public void setSeuil_min_NO2(int seuil_min_NO2) {
		this.seuil_min_NO2 = seuil_min_NO2;
	}

	public int getSeuil_max_NO2() {
		return seuil_max_NO2;
	}

	public void setSeuil_max_NO2(int seuil_max_NO2) {
		this.seuil_max_NO2 = seuil_max_NO2;
	}

	public int getSeuil_min_SO2() {
		return seuil_min_SO2;
	}

	public void setSeuil_min_SO2(int seuil_min_SO2) {
		this.seuil_min_SO2 = seuil_min_SO2;
	}

	public int getSeuil_max_SO2() {
		return seuil_max_SO2;
	}

	public void setSeuil_max_SO2(int seuil_max_SO2) {
		this.seuil_max_SO2 = seuil_max_SO2;
	}

	public int getSeuil_min_PM25() {
		return seuil_min_PM25;
	}

	public void setSeuil_min_PM25(int seuil_min_PM25) {
		this.seuil_min_PM25 = seuil_min_PM25;
	}

	public int getSeuil_max_PM25() {
		return seuil_max_PM25;
	}

	public void setSeuil_max_PM25(int seuil_max_PM25) {
		this.seuil_max_PM25 = seuil_max_PM25;
	}

	public int getTempérature() {
		return température;
	}

	public void setTempérature(int température) {
		this.température = température;
	}

	public Timestamp getDate_time() {
		return date_time;
	}

	public void setDate_time(Timestamp date_time) {
		this.date_time = date_time;
	}
	
	
	

}
