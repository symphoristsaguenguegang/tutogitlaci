package fr.episen.pds.ing2.techcity.services;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.episen.pds.ing2.techcity.models.Historique_Capteur;
import fr.episen.pds.ing2.techcity.models.User;
import fr.episen.pds.ing2.techcity.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	 @Autowired
	    private UserRepository userRepository;

	   
	    public List < User > getAllUsers() {
	        return userRepository.findAll();
	    }

	    public void saveUser(User user) {
	        this.userRepository.save(user);
	    }

	    public User getUserById(long id) {
	        Optional < User > optional = userRepository.findById(id);
	        User user = null;
	        if (optional.isPresent()) {
	            user = optional.get();
	        } else {
	            throw new RuntimeException(" User not found for id :: " + id);
	        }
	        return user;
	    }

	    public void deleteUserById(long id) {
	        this.userRepository.deleteById(id);
	    }

		
}
