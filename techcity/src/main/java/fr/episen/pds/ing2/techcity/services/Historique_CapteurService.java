package fr.episen.pds.ing2.techcity.services;

import java.util.List;

import fr.episen.pds.ing2.techcity.models.Historique_Capteur;

public interface Historique_CapteurService {

	List <Historique_Capteur> getAllHistorique_Capteur();
}
