package fr.episen.pds.ing2.techcity.services;

import java.util.List;

import fr.episen.pds.ing2.techcity.models.Historique_Capteur;
import fr.episen.pds.ing2.techcity.models.User;

public interface UserService {
	
	 List < User > getAllUsers();
	 void saveUser(User user);
	 User getUserById(long id);
	 void deleteUserById(long id);
	 

}
