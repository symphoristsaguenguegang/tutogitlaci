package fr.episen.pds.ing2.techcity.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.episen.pds.ing2.techcity.models.Historique_Capteur;
import fr.episen.pds.ing2.techcity.repositories.Historique_CapteurRepository;
import fr.episen.pds.ing2.techcity.repositories.UserRepository;

@Service
public class Historique_CapteurImpl implements Historique_CapteurService {

	@Autowired
    private Historique_CapteurRepository historique_CapteurRepository;
	
	
	public List<Historique_Capteur> getAllHistorique_Capteur() {
		
		
		return historique_CapteurRepository.findAll();
		
		
	}

}
