package fr.episen.pds.ing2.techcity.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.episen.pds.ing2.techcity.models.Historique_Capteur;
import fr.episen.pds.ing2.techcity.repositories.Historique_CapteurRepository;
import fr.episen.pds.ing2.techcity.services.Historique_CapteurService;

@Controller
@RequestMapping(value = "hist", produces = {"application/json"})
public class HistoriqueCapteurController {

	private Historique_CapteurRepository historiqueCapteurRepository;
	@Autowired
	private Historique_CapteurService historiqueCapteurService;


	@GetMapping
	public String findAllHistorique_Capteur(Model model){
		List<Historique_Capteur> liste = historiqueCapteurService.getAllHistorique_Capteur();

		List<Historique_Capteur> result = new ArrayList<Historique_Capteur>();
		for (Historique_Capteur historique_Capteur : liste) {

			if(historique_Capteur.getSeuil_max_CO2()>386) { 

				result.add(historique_Capteur);  

				model.addAttribute("list",liste);	


			}
		}
		return "list";
	}
}

