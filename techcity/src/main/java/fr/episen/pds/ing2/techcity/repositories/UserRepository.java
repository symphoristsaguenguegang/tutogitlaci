package fr.episen.pds.ing2.techcity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.episen.pds.ing2.techcity.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
}
