package fr.episen.pds.ing2.techcity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechcityApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechcityApplication.class, args);
	}

}
